package Ce;

import java.util.HashMap;
import java.util.Map;

public class Frequentauthor {

	public static void FrequentWord(String array[]) 
	{ 
		Map<String,Integer> map = new HashMap<String, Integer>(); 
		for (String str:array) 
		{ 
			if (map.keySet().contains(str))
				map.put(str,map.get(str)+1); 
			else
				map.put(str,1);
		} 
		String max=""; 
    	int val=0; 
		for (Map.Entry<String,Integer> entry:map.entrySet()) 
		{	String key=entry.getKey(); 
			Integer count=entry.getValue(); 
			if (count>val) 
			{ 
				val=count; 
				max=key; 
			} 
			else if (count==val){ 
				if (key.length()<max.length())
					max=key; 
			}
		} 
		System.out.println("Nejv�c citatu ma: "+max); 
	} 	
	

}
